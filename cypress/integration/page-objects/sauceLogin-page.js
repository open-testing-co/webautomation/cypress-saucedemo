import { isNotEmpty } from "../helpers/functions"

export class SauceLoginPage {
    login(user, pass, messageError) {
        if (isNotEmpty(user))
            cy.get('[data-test=username]').type(user)
        if (isNotEmpty(pass))
            cy.get('[data-test=password]').type(pass)
        cy.get('.btn_action').click()
        if (messageError.toLowerCase() == 'success') {
            cy.get('.product_label')
        }
        else {
            cy.get('[data-test=error]').should('contain.text', messageError)
        }
    }
}