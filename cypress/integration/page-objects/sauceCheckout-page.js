import { isNotEmpty } from "../helpers/functions"

export class SauceCheckoutPage {
    checkout(firstName, lastName, zipCode, message) {
        cy.get('#shopping_cart_container path').click()
        cy.get('.btn_action').click()
        if (isNotEmpty(firstName))
            cy.get('[data-test=firstName]').type(firstName)
        if (isNotEmpty(lastName))
            cy.get('[data-test=lastName]').type(lastName)
        if (isNotEmpty(zipCode))
            cy.get('[data-test=postalCode]').type(zipCode)
        cy.get('.btn_primary').click()

        if (message.toLowerCase() == 'checkout') {
            cy.get('.subheader').should('contain.text', message)
        }
        else {
            cy.get('[data-test=error]').should('contain.text', message)
        }
    }
}