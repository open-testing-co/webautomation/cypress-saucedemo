export class SaucePage {
    navigate() {
        cy.visit('https://www.saucedemo.com/')
    }

    buy() {
        cy.get('.btn_action').click()
        cy.get('.subheader').should('have.text', 'Finish')
    }

}
