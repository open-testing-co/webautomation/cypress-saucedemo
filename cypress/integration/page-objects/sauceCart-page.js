export class SauceCartPage {


    addToCart(item) {
        cy.contains('div', item).click()
        cy.get('.btn_primary').click()
        cy.get('.inventory_details_back_button').click()
    }

    removeToCart(item) {
        cy.get('#shopping_cart_container path').click()
        cy.contains('.cart_item_label', item).within(() => {
            cy.get('.item_pricebar > .btn_secondary').click()
        })
        // The next code has the same behavior                  */
        //** cy.contains('div', item).click()                   */
        //** cy.get('.btn_secondary').click()                   */
        //** cy.get('.inventory_details_back_button').click()   */
    }

    verityCart(item) {
        cy.get('#shopping_cart_container path').click()
        cy.get('.inventory_item_name').should('have.text', item)
        cy.get('.item_pricebar > .btn_secondary').click()
    }

}