export function isNotEmpty(str) {
    return !(!str || 0 === str.length);
}