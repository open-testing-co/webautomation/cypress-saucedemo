/// <reference types="Cypress" />

import { SaucePage } from "./page-objects/sauce-page"
import { SauceLoginPage } from "./page-objects/sauceLogin-page"
import { SauceCheckoutPage } from "./page-objects/sauceCheckout-page"
import { SauceCartPage } from "./page-objects/sauceCart-page"

describe('Sauce Demo Testing', function () {
    const saucePage = new SaucePage
    const loginPage = new SauceLoginPage
    const checkoutPage = new SauceCheckoutPage
    const cart = new SauceCartPage

    beforeEach('Navigate', () => {
        saucePage.navigate()
    })

    const users = [
        { user: 'user', pass: 'pass', message: 'not match' },
        { user: '', pass: 'pass', message: 'Username is required' },
        { user: 'user', pass: '', message: 'Password is required' },
        { user: '', pass: '', message: 'Username is required' },
        { user: 'standard_user', pass: 'secret_sauce', message: 'Success' },
        { user: 'locked_out_user', pass: 'secret_sauce', message: 'locked out' },
        { user: 'problem_user', pass: 'secret_sauce', message: 'Success' },
        { user: 'performance_glitch_user', pass: 'secret_sauce', message: 'Success' },
    ]
    users.forEach(function (users) {
        it('Login ' + users.user + ' ' + users.pass, () => {
            loginPage.login(users.user, users.pass, users.message)
        })
    });

    it('E2E test to buy two products', () => {
        loginPage.login('performance_glitch_user', 'secret_sauce', 'Success')
        cart.addToCart('Sauce Labs Onesie')
        cart.addToCart('Sauce Labs Bike Light')
        checkoutPage.checkout('Henry', 'Correa', '050010', 'Checkout')
        saucePage.buy()
    })

    it('Verify the labels in the Your Cart page', () => {
        loginPage.login('standard_user', 'secret_sauce', 'Success')
        cart.addToCart('Sauce Labs Backpack')
        cart.verityCart('Sauce Labs Backpack')
    })

    it('Add and remove item', () => {
        loginPage.login('standard_user', 'secret_sauce', 'Success')
        cart.addToCart('Sauce Labs Backpack')
        cart.removeToCart('Sauce Labs Backpack')
    })

    const chekout = [
        { firstName: 'Henry', lastName: 'Correa', zip: '050010', message: 'Checkout' },
        { firstName: '', lastName: 'Correa', zip: '050010', message: 'First Name is required' },
        { firstName: 'Henry', lastName: '', zip: '050010', message: 'Last Name is required' },
        { firstName: 'Henry', lastName: 'Correa', zip: '', message: 'Postal Code is required' },
    ]
    chekout.forEach(function (chekout) {
        it('Validations for the Checkout your information fields ' + chekout.message, () => {
            loginPage.login('standard_user', 'secret_sauce', 'Success')
            cart.addToCart('Sauce Labs Backpack')
            checkoutPage.checkout(chekout.firstName, chekout.lastName, chekout.zip, chekout.message)
            cart.removeToCart('Sauce Labs Backpack')
        })
    });

})