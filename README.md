# cypress Saucedemo

Cypress is a JavaScript test automation tool, similar to Selenium WebDriver, WebDriverIO, TestCafe, and others.
- Only for Testing
- Only JavaScript
- Only Chrome
- Only Mocha

## Prerequisites
- NPM
- NodeJS

## Commands
```npm install cypress@4.2.0 --save-dev``` for install cypress
```npx cypress open``` for running cypress locally in your test folder 
```npx cypress run``` for running cypress headless mode
```npx cypress run --spec cypress\integration\google.spec.js``` for run an specific spect

## Credits
Author: Henry Andres Correa Correa